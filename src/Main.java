import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;

import tools.Commander;



public class Main {

	public static void main (String[] args) throws Exception {
		String host = "127.0.0.1"; 
		int port = 5555; 
		
		File file = new File("test.txt");
		FileOutputStream fis = new FileOutputStream(file);
		PrintStream o = new PrintStream(fis);
		System.setOut(o);
		
		try{
			host = args[0]; 
			port = Integer.parseInt(args[1]); 
		}catch(Exception e){
			e.printStackTrace();
		}
		
		OutputStream out = null;
		InputStream in = null;
		Socket socket = null;
		Commander cmd = new Commander();
		
		
		try {
			socket = new Socket(host, port);
			in = socket.getInputStream();
			out = socket.getOutputStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		cmd.sendTeamName(out);
		
		
		//d�but des �changes
		while (true) {
            boolean isByeTrame = cmd.read(in, out);
            if (isByeTrame) {
                break;
            }
        }
		
		//fermeture de tout
		in.close();
        out.close();
        socket.close();
		
	}
    



}
