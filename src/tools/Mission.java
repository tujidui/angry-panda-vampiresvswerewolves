package tools;


public class Mission {
	public int initX;		// the init x coord
	public int initY;		// the init y coord
	public int x;			// the current x coord of the group
	public int y;			// the current y coord of the group
	public int toX;			// the x coord of the target point
	public int toY;			// the y coord of the target point
	public int number;		// the number of individuals in the group
	public int missionCode;	// the mission's type
	public int minNumber;	// the minimum number of individuals necessary to achieve this mission
	private boolean isAchieved = false;
	public int targetIndex;
	public int population;
	
	public Mission(int x, int y, int number, int toX, int toY, int missionCode, int minNumber) {
		super();
		this.x = x;
		this.y = y;
		this.initX = x;
		this.initY = y;
		this.toX = toX;
		this.toY = toY;
				
		this.number = number;
		this.missionCode = missionCode;
		this.minNumber = minNumber;
	}
	
	public void achieve(){
		isAchieved = true;
	}

	public Mission(Mission mission) {
		this.initX = mission.initX;
		this.initY = mission.initY;
		this.x = mission.x;
		this.y = mission.y;
		this.toX = mission.toX;
		this.toY = mission.toY;
		
		this.number = mission.number;
		this.missionCode = mission.missionCode;
		this.minNumber = mission.minNumber;
	}

	public Mission(int x, int y, int number, int toX, int toY, int missionCode, int minNumber, int targetIndex, int population) {
		super();
		this.x = x;
		this.y = y;
		this.initX = x;
		this.initY = y;
		this.toX = toX;
		this.toY = toY;
		this.number = number;
		this.missionCode = missionCode;
		this.minNumber = minNumber;
		this.targetIndex = targetIndex;
		this.population = population;
	}
	
	public boolean hasMovingTarget(){
		return (missionCode == Const.MISSION_FUSION)||(missionCode == Const.MISSION_ENEMY);
	}

	public boolean isAchieved(){
//		if(missionCode == Const.MISSION_IDLE){
//			return false;
//		}
		return (y == toY) && (x == toX);
	}


	public void move(){
		//TODO : implement map changes		
		if(x > toX && y > toY){
			x--;
			y--;
		} else if(x < toX && y < toY){
			x++;
			y++;
		} else if(x < toX && y > toY){
			x++;
			y--;
		} else if(x > toX && y < toY){
			x--;
			y++;
		} else if(x < toX && y == toY){
			x++;
		} else if(x > toX && y == toY){
			x--;
		} else if(x == toX && y < toY){
			y++;
		} else if(x == toX && y > toY){
			y--;
		}
	}
	
	public int getNumber() {
		return number;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Mission [" + x + "," + y + ", to " + toX + "," + toY
				+ ", n=" + number + ", missionCode=" + missionCode + "]";
	}

	public int getMinimumNumber() {
		return minNumber;
	}

	public void setNumber(int n) {
		this.number = n;
	}

	/**
	 * @return the type
	 */
	public int getMissionCode() {
		return missionCode;
	}

	/**
	 * @param code the type to set
	 */
	public void setMissionCode(int code) {
		this.missionCode = code;
	}
	
	

}
