package tools;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import Graph.MMGraph;
import Map.DefaultMap;


public class Commander {
	private DefaultMap map;

	//Envoi du nom
	public boolean sendTeamName(OutputStream out){
		byte[] trame = ("NME" + "N" + Const.TEAM_NAME).getBytes();
		trame[3] = (byte)Const.TEAM_NAME_LENG;
		Const.print("Sending team name : "+ Const.TEAM_NAME + trame);
		return write(trame, out);
	}
	
	//send moves : x,y,number,toX,toY
	public boolean sendMove(OutputStream out, ArrayList<int[]> arrayList){
		int listSize = arrayList.size();
		int leng = 4 + arrayList.size()*5;
		byte[] trame = new byte[leng];
		System.arraycopy(("MOV").getBytes(), 0, trame, 0, 3);
		
		trame[3] = (byte) listSize;
		
		Const.print("");
		
		Const.print("***********************************************************");
		
		Const.print("Sending moves : " + arrayList.size());
		
		for(int i = 0; i<listSize; i++){
			Const.print(String.format("Sending move : (%d,%d) %d to (%d,%d)",arrayList.get(i)[0],arrayList.get(i)[1],arrayList.get(i)[2],arrayList.get(i)[3],arrayList.get(i)[4]));
			trame[4+i*5] =  (byte)arrayList.get(i)[0];
			trame[4+i*5+1] =  (byte)arrayList.get(i)[1];
			trame[4+i*5+2] =  (byte)arrayList.get(i)[2];
			trame[4+i*5+3] =  (byte)arrayList.get(i)[3];
			trame[4+i*5+4] =  (byte)arrayList.get(i)[4];
		}
		
		 Const.print("******************** Sending moves : end *********************");
		 Const.print("");
			
		 return write(trame, out);
	}
	
	//write and send informations to the server
	 private boolean write(byte[] trame, OutputStream out) {
	        try {
	            out.write(trame, 0, trame.length);
	            return true;
	        } catch (Exception e) {
	            Const.print("sendTeamName : Erreur d'�criture de la trame envoy��e ");
	            return false;
	        }
			
		}
	
	
	// M�thode qui permet de traiter les trames
    public boolean read(InputStream in, OutputStream out) throws Exception {
	
		//----------------lecture des 3 premiers octets
        byte[] trame = new byte[3];
        int nbBytesLus = in.read(trame, 0, 3);
        if (nbBytesLus != 3) {
            throw new Exception("Erreur de lecture de l'ent�te de trame");
        }
        String typeTrame = new String(trame, "ASCII");
        
		//-----------------action en fonction de la nature de la trame
        if (typeTrame.equalsIgnoreCase("SET")) {
            trame = new byte[2];
            nbBytesLus = in.read(trame, 0, 2);
            Const.print("SET command received ");
            if (nbBytesLus != 2) {
                throw new Exception("Erreur de lecture de la trame SET");
            } 
            createMap(trame[0], trame[1]);
			
        //-----------------Human cases initialization
        } else if (typeTrame.equalsIgnoreCase("HUM")) {
            trame = new byte[1];
            nbBytesLus = in.read(trame, 0, 1);
            Const.print("HUM command received ");
            
            if (nbBytesLus != 1) {
                throw new Exception("Erreur de lecture de N de la trame HUM");
            }
            int N = (int) trame[0] & 0xff;
            trame = new byte[2 * N];
            nbBytesLus = in.read(trame, 0, 2 * N);
            if (nbBytesLus != 2 * N) {
                throw new Exception("Erreur de lecture des donn�es de la trame HUM");
            }
            map.setHumans(N,trame);
            
		//---------------set the start case !
        } else if (typeTrame.equalsIgnoreCase("HME")) {
            trame = new byte[2];
            nbBytesLus = in.read(trame, 0, 2);
            Const.print("HME command received ");
            if (nbBytesLus != 2) {
                throw new Exception("Erreur de lecture de la trame HME");
            }
            map.setStartCase(trame);
			
        //-----------------receive the init map
        } else if (typeTrame.equalsIgnoreCase("MAP")) {
            trame = new byte[1];
            nbBytesLus = in.read(trame, 0, 1);
            Const.print("MAP command received ");
            if (nbBytesLus != 1) {
                throw new Exception("Erreur de lecture de N de la trame MAP");
            }
            int N = (int) trame[0] & 0xff;
            trame = new byte[5 * N];
            nbBytesLus = in.read(trame, 0, 5 * N);
            if (nbBytesLus != 5 * N) {
                throw new Exception("Erreur de lecture des donn�es de la trame MAP");
            }
            map.update(N,trame);
        // ------------------update map cases
        } else if (typeTrame.equalsIgnoreCase("UPD")) {
            trame = new byte[1];
            nbBytesLus = in.read(trame, 0, 1);
            Const.print("UPD command received ");
            if (nbBytesLus != 1) {
                throw new Exception("Erreur de lecture de N de la trame UPD");
            }
            int N = (int) trame[0] & 0xff;
            trame = new byte[5 * N];
            nbBytesLus = in.read(trame, 0, 5 * N);
            if (nbBytesLus != 5 * N) {
                throw new Exception("Erreur de lecture des donn�es de la trame UPD");
            }
            
            

            map.update(N,trame);
			
			//-------------------------------calculer le prochain coup ici
            //sendMove(out,new int[][]{new int[]{5,4,2,4,4}}); ===> x,y,number, tox, toy
            
            MMGraph graph = new MMGraph(map);
            sendMove(out, graph.getNextMove());
            graph = null;
            System.gc();
            
        } else if (typeTrame.equalsIgnoreCase("END")) {
        	Const.print("END command received ");
            endGame();
        } else if (typeTrame.equalsIgnoreCase("BYE")) {
        	Const.print("BYE command received ");
            return true;
        } else {
            throw new Exception("Trame non reconnue");
        }
        return false;
    }

	private void endGame() {
		// TODO : ...ajouter ici les actions a  faire en fin de partie
	}


	private void createMap(byte line, byte col) {
		Const.print("creating a map : " + (int)line + (int)col);
		map = new DefaultMap((int)line, (int)col);
		
	}

}
