package tools;

//Global constants

public class Const {
	
	public static final String TEAM_NAME = "AngryPanda";
	public static final int TEAM_NAME_LENG = TEAM_NAME.length();
	public static final boolean DEBUG_MODE = false;
	public static final long LIMIT_TIME = 1500;	
	public static final int FLOOR_MAX = 10; 		//develop the decision graph until level 10
	
	public static final int VAMPIRE = 0;	
	public static final int WWOLF = 1;
	
	public static final int SCORE_MAX = 99999; 	// le score maximal symbolisant la victoire du joueur courant
	public static final int SCORE_MIN = -99999; // le score minimal symbolisant la d閒aite du joueur courant
	

	public static final int MISSION_ENEMY = 0;	
	public static final int MISSION_HUMANS = 1;
	
	public static final int MISSION_FUSION = 4;
	public static final int MISSION_IDLE = 5;
	
	
	
	public static final void print(String s){
		if(DEBUG_MODE){
			System.out.println(s);
		}
	}
	

}
