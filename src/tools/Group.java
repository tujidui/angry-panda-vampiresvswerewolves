package tools;

public class Group {
	public int x,y,number;

	public Group(int x, int y, int number) {
		super();
		this.x = x;
		this.y = y;
		this.number = number;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[" + x + "," + y + ", n=" + number + "]";
	}
	

}
