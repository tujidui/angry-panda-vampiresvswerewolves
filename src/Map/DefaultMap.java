package Map;
import java.util.ArrayList;
import java.util.Arrays;

import tools.Const;
import tools.Group;
import tools.Mission;


//First implementation of map's structure : combining matrix and group lists
public class DefaultMap extends Map implements Cloneable{
	private ArrayList<Group> list_humans, list_werewolves, list_vampires; 
	private int[][][] map_matrix; // ordre dans une case : vampire, werewolves, humans
	private int[] sum = new int[]{0,0};
	public final int maxY, maxX;

	
	public DefaultMap(DefaultMap map){
		this.maxY = map.maxY;
		this.maxX = map.maxX;
		map_matrix = new int[map.map_matrix.length][map.map_matrix[0].length][3];
		int[][][] temp = map.getMap_matrix();
		for(int i=0; i<map_matrix.length; i++)
			  for(int j=0; j<map_matrix[i].length; j++)
				  for(int k=0; k<3; k++)
					  map_matrix[i][j][k]=temp[i][j][k];
//		
		//map_matrix =  Arrays.copyOf(map.getMap_matrix(), map.getMap_matrix().length);
		list_humans = new ArrayList<Group>(map.getHumanList());
		list_vampires = new ArrayList<Group>(map.getList_vampires());
		list_werewolves = new ArrayList<Group>(map.getList_werewolves());
		sum = new int[2];
		sum[0] = map.getSum()[0];
		sum[1] = map.getSum()[1];
		role = map.role;
	}
	
	public DefaultMap(int line, int col) {
		this.maxY = line-1;
		this.maxX = col-1;
		map_matrix = new int[col][line][3];
		list_humans = new ArrayList<Group>();
		list_vampires = new ArrayList<Group>();
		list_werewolves = new ArrayList<Group>();
	}
	
	//x,y,number
	public ArrayList<Group> getEnemyList(int role){
		if(role == Const.WWOLF){
			return list_vampires;
		} else {
			return list_werewolves;
		}
	}
	
	//x,y,number
	public ArrayList<Group> getAllyList(int role){
		if(role == Const.VAMPIRE){
			return list_vampires;
		} else {
			return list_werewolves;
		}
	}
	
	//x,y,number
	public ArrayList<Group> getHumanList(){
		return list_humans;
	}

	// X,Y,humanNB, vampires, werewolves
	void updateCase(int x, int y, int hum, int vamp, int were) {
		Const.print(String.format("       updating (%d,%d) hum : %d, vamp %d, ww %d", x,y,hum,vamp,were));
		list_humans = updateList(x,y,hum,list_humans);
		
		sum[Const.VAMPIRE] += vamp - map_matrix[x][y][0];
		list_vampires = updateList(x,y,vamp,list_vampires);
		
		sum[Const.WWOLF] += were - map_matrix[x][y][1];
		list_werewolves = updateList(x,y,were,list_werewolves);
		
		map_matrix[x][y]= new int[]{vamp,were,hum};
		
	}
	
	//update a case's stat in the list map
	private ArrayList<Group> updateList(int x, int y, int number, ArrayList<Group> list) {
		//TODO : ? optimisation ?
		for(int i  = 0; i<list.size(); i++){
			if(list.get(i).x == x && list.get(i).y == y){
				list.remove(i);
				i--;
			}
		}
		
		//si le nombre d'individu final est strictement positif, alors ajoute l'� at courant � la liste
		if(number > 0){
			list.add(new Group(x,y,number));
			//Const.print("the new number added is :  x :" + x + ", y : " + y + ", number : " + number);
		}
		return list;
		
	}

	/**
	 * @return the sum
	 */
	public int[] getSum() {
		return sum;
	}


	public float getLocalScore() {
		int result = 0;
		for(Group ally : getAllyList(role)){
			int sum1 = 0;
			for(Group a : getAllyList(role)){
				if(Math.abs(a.x - ally.x) <=2  && Math.abs(a.y - ally.y) <=2){
					sum1 += a.number;
				}
			} for(Group e : getEnemyList(role)){
				if(Math.abs(e.x - ally.x) <=2  && Math.abs(e.y - ally.y) <=2){
					sum1 -= e.number;
				}
			}
			result += sum1;
		}
		return result;
	}

	 public float getDistanceScore() {
	        int sum = 0;

	        for(Group human : list_humans){
	            int shortest_distance_vampire = Integer.MAX_VALUE;
	            int shortest_distance_wwolf = Integer.MAX_VALUE;
	            for(Group vampire : list_vampires){
	                int distance = getDistance(vampire.x, vampire.y, human.x, human.y);
	                if(distance < shortest_distance_vampire && vampire.number > human.number){
	                    shortest_distance_vampire = distance;
	                }
	            }
	            for(Group wwolf : list_werewolves){
	                int distance = getDistance(wwolf.x, wwolf.y, human.x, human.y);
	                if(distance < shortest_distance_wwolf && wwolf.number > human.number){
	                    shortest_distance_wwolf = distance;
	                }
	            }
	            if(role == Const.VAMPIRE){
	                if(shortest_distance_vampire < shortest_distance_wwolf){
	                    sum += human.number;
	                }
	                else if(shortest_distance_vampire > shortest_distance_wwolf){sum -= human.number;}
	            }
	            else{
	                if(shortest_distance_wwolf < shortest_distance_vampire){
	                    sum += human.number;
	                }
	                else if (shortest_distance_vampire > shortest_distance_wwolf){sum -= human.number;}
	            }
	        }

	        return sum;
	    }

	private int getDistance(int x, int y, int x2, int y2) {
		return Math.max(Math.abs(x-x2), Math.abs(y-y2));
	}
	
	public boolean isValide(int x, int y, int role, int number){
		ArrayList<Group> allyList = getEnemyList(role); // the ally list in fact... =.=
		for(Group g : allyList){
			if(g.x == x && g.y == y){
				//System.out.println(String.format("-------(g.x, g.y, x, y): %d %d %d %d", g.x, g.y, x, y));
				return false;
			}			
		}
		
		for(Group g : getAllyList(role)){ // the enemy list in fact T.T 
			if(g.x == x && g.y == y && g.number >= number){
				return false;
			}
		}
		

		return true;	
	}
	
	public int[] tryMove(Mission m, int role){
		int initX = m.x;
		int initY = m.y;
		int x = m.x;
		int y = m.y;
		int toX = m.toX;
		int toY = m.toY;
		int num = m.getNumber();
		
		
		//System.out.println(String.format("-------(current x, current y): %d %d %d %d", x, y));

		if(x > toX && y > toY){
			if((isValide(x-1, y-1,role,num))){
				x--;
				y--;
			}
			else if(isValide(x-1, y, role,num)){
				x--;
			}
			else if(isValide(x, y-1, role,num)){
				y--;
			}
			//TODO:
			else{}
			
		} else if(x < toX && y < toY){
			if(isValide(x+1, y+1, role,num)){
				x++;
				y++;	
			}
			else if(isValide(x+1, y, role,num)){
				x++;
			}
			else if(isValide(x, y+1, role,num)){
				y++;
			}
			
			//TODO:
			else{}
		} else if(x < toX && y > toY){
			if(isValide(x+1, y-1, role,num)){
				x++;
				y--;	
			}
			else if(isValide(x+1, y, role,num)){
				x++;
			}
			else if(isValide(x, y-1, role,num)){
				y--;
			}
			
			//TODO:
			else{}
			
		} else if(x > toX && y < toY){
			if(isValide(x-1, y+1, role,num)){
				x--;
				y++;	
			}
			else if(isValide(x-1, y, role,num)){
				x--;
			}
			else if(isValide(x, y+1, role,num)){
				y++;
			}
			
			//TODO:
			else{}
			
			
		} else if(x < toX && y == toY){
			if(isValide(x+1, y, role,num)){
				x++;	
			}
			else if(isValide(x+1, y+1, role,num) && y< maxY){
				x++;
				y++;
			}
			else if(isValide(x+1, y-1, role,num) && y>0){
				x++;
				y--;
			}
			
			//TODO:
			else{}
			
		} else if(x > toX && y == toY){
			if(isValide(x-1, y, role,num)){
				x--;	
			}
			else if(isValide(x-1, y+1, role,num) && y<maxY){
				x--;
				y++;
			}
			else if(isValide(x-1, y-1, role,num) && y>0){
				x--;
				y--;
			}
			
			//TODO:
			else{}
			
		} else if(x == toX && y < toY){
			if(isValide(x, y+1, role,num)){
				y++;	
			}
			else if(isValide(x+1, y+1, role,num) && x<maxX){
				x++;
				y++;
			}
			else if(isValide(x-1, y+1, role,num) && x>0){
				x--;
				y++;
			}
			
			//TODO:
			else{}
		} else if(x == toX && y > toY){
			if(isValide(x, y-1, role,num)){
				y--;	
			}
			else if(isValide(x+1, y-1, role,num) && x< maxX){
				x++;
				y--;
			}
			else if(isValide(x-1, y-1, role,num) && x >0){
				x--;
				y--;
			}
			
			//TODO:
			else{}
		}	
		
		//System.out.println(String.format("-------(next x, next y): %d %d %d %d", x, y));

		int[] result = {initX, initY, x, y, num}; 
		return result;
	}
	
	public boolean move(Mission[] missions, int role) {
		boolean hasModifiedMissions = false;
		ArrayList<int[]> moveList = new ArrayList<int[]>();
		
		for(Mission m : missions){
			if(m.missionCode != Const.MISSION_FUSION){
				int[] newMove = tryMove(m, role);
				moveList.add(newMove);
				m.x = newMove[2];
				m.y = newMove[3];
			}
		}
		
		for(Mission m : missions){
			if(m.missionCode == Const.MISSION_FUSION){
				for(int[] move : moveList ){
					if(move[0] == m.toX && move[1] == m.toY){
						m.toX = move[2];
						m.toY = move[3];
						break;
					}
				}
				int[] newMove = tryMove(m, role);
				moveList.add(newMove);
				m.x = newMove[2];
				m.y = newMove[3];
			}
		}
		
		for(int i = 0 ; i < moveList.size() ; i++){
			int x = moveList.get(i)[0];
			int y = moveList.get(i)[1];
			int x2 = moveList.get(i)[2];
			int y2 = moveList.get(i)[3];
			int number = moveList.get(i)[4];
			
			Const.print(String.format("\n[MOVE] (%d, %d) %d to (%d, %d)\n", x,y,number,x2,y2));
			
			if(role == Const.WWOLF){
				//matrix : vampire + wwolves + human     method : hum, int vamp, int were
				Const.print("-------------------------Vampires' move-------------------");
				Const.print(String.format("[INFO] case (%d,%d), humans : %d, vampires : %d, wwolves : %d", x,y,map_matrix[x][y][2],map_matrix[x][y][0],map_matrix[x][y][1]));
				updateCase(x,y,map_matrix[x][y][2],map_matrix[x][y][0]-number,map_matrix[x][y][1]);
				updateCase(x2,y2,map_matrix[x2][y2][2],map_matrix[x2][y2][0]+number,map_matrix[x2][y2][1]);
			} else {
				Const.print("------------------------Wolves' move-------------------");
				Const.print(String.format("[INFO] case (%d,%d), humans : %d, vampires : %d, wwolves : %d", x,y,map_matrix[x][y][2],map_matrix[x][y][0],map_matrix[x][y][1]));
				updateCase(x,y,map_matrix[x][y][2],map_matrix[x][y][0],map_matrix[x][y][1]-number);
				updateCase(x2,y2,map_matrix[x2][y2][2],map_matrix[x2][y2][0],map_matrix[x2][y2][1]+number);
			}
			
			if(resolveSimulatedAction()){
				hasModifiedMissions = true;
			}
			
			Const.print(toString());
			
		}
	
		return hasModifiedMissions;
	}

	
//	public boolean move(Mission[] missions, int role) {
//		boolean hasModifiedMissions = false;
//		for(Mission m : missions){
//			int x = m.x;
//			int y = m.y;
//			int number = m.getNumber();
//			m.move();
//			int x2 = m.x;
//			int y2 = m.y;
//			
//			Const.print(String.format("\n[MOVE] (%d, %d) %d to (%d, %d)\n", x,y,number,x2,y2));
//			
//			//x,y,h,w,v
//			if(role == Const.WWOLF){
//				//matrix : vampire + wwolves + human     method : hum, int vamp, int were
//				Const.print("-------------------------Vampires' move-------------------");
//				Const.print(String.format("[INFO] case (%d,%d), humans : %d, vampires : %d, wwolves : %d", x,y,map_matrix[x][y][2],map_matrix[x][y][0],map_matrix[x][y][1]));
//				updateCase(x,y,map_matrix[x][y][2],map_matrix[x][y][0]-number,map_matrix[x][y][1]);
//				updateCase(x2,y2,map_matrix[x2][y2][2],map_matrix[x2][y2][0]+number,map_matrix[x2][y2][1]);
//			} else {
//				Const.print("------------------------Wolves' move-------------------");
//				Const.print(String.format("[INFO] case (%d,%d), humans : %d, vampires : %d, wwolves : %d", x,y,map_matrix[x][y][2],map_matrix[x][y][0],map_matrix[x][y][1]));
//				updateCase(x,y,map_matrix[x][y][2],map_matrix[x][y][0],map_matrix[x][y][1]-number);
//				updateCase(x2,y2,map_matrix[x2][y2][2],map_matrix[x2][y2][0],map_matrix[x2][y2][1]+number);
//			}
//			
//			//Eat people ! And if there is a modification in population numbers, set the return value to true
//			if(resolveSimulatedAction()){
//				hasModifiedMissions = true;
//			}
//			
//			Const.print(toString());
//		}
//		
//		return hasModifiedMissions;
//	}

	//return if there is a modification in population numbers
	private boolean resolveSimulatedAction() {
		boolean hasModifcation = false;
		// TODO : !!Optimization !!
		// verify for 
		for(int v = 0; v < list_vampires.size(); v++){
			Group vampire = list_vampires.get(v);
			
			for(int w = 0; w < list_werewolves.size(); w++){
				Group wwol = list_werewolves.get(w);
				if(vampire.x == wwol.x && vampire.y == wwol.y ){
					if(vampire.number >= 1.5 * wwol.number){
						Const.print("[BATTLE!] vampires eat wolves");
						hasModifcation = true;
						updateCase(vampire.x, vampire.y, 0, vampire.number, 0);
					} else if(wwol.number >= 1.5 * vampire.number){
						Const.print("[BATTLE!] wolves eat vampires");
						updateCase(vampire.x, vampire.y, 0, 0, wwol.number);
						hasModifcation = true;
					} else {
						if(ifWin(vampire.number,wwol.number)){
							Const.print("[BATTLE!] vampires win the random battle !");
							updateCase(vampire.x, vampire.y, 0, getWinnerNumber(vampire.number, wwol.number, false), 0);
							hasModifcation = true;
							v--;
						} else {
							Const.print("[BATTLE!] wolves win the random battle !");
							updateCase(vampire.x, vampire.y, 0, 0, getWinnerNumber(wwol.number, vampire.number, false));
							hasModifcation = true;
							w--;
						}
					}
				}
			}
			for(int h = 0; h < list_humans.size(); h++){
				Group human = list_humans.get(h);
				if(vampire.x == human.x && vampire.y == human.y ){
					if(vampire.number >= human.number){
						Const.print("[BATTLE!] vampires convert humans");
						updateCase(vampire.x, vampire.y, 0, vampire.number + human.number, 0);
						hasModifcation = true;
					} else {
						if(ifWin(vampire.number,human.number)){
							Const.print("[BATTLE!] vampire convert humans in random battle !");
							updateCase(vampire.x, vampire.y, 0, getWinnerNumber(vampire.number,human.number, true),0 );
							hasModifcation = true;
							h--;
						} else {
							Const.print("[BATTLE!] human killed vampires in random battle");
							updateCase(vampire.x, vampire.y, getWinnerNumber(human.number,vampire.number, true), 0, 0);
							hasModifcation = true;
							v--;
						}

					}
				}

			}
		}
		
		
		for(int w = 0; w < list_werewolves.size(); w++){
			Group wwol = list_werewolves.get(w);
			for(int h = 0; h < list_humans.size(); h++){
				Group human = list_humans.get(h);
				if(wwol.x == human.x && wwol.y == human.y ){
					if(wwol.number >= human.number){
						Const.print("[BATTLE!] wolves convert humans !");
						updateCase(wwol.x, wwol.y, 0, 0, wwol.number + human.number);
						hasModifcation = true;
					} else {
						if(ifWin(wwol.number,human.number)){
							Const.print("[BATTLE!] wolves convert humans in random battle");
							updateCase(wwol.x, wwol.y, 0, 0, getWinnerNumber(wwol.number,human.number, true));
							hasModifcation = true;
							h--;
						} else {
							Const.print("[BATTLE!] human killed wolves in random battle");
							updateCase(wwol.x, wwol.y, getWinnerNumber(human.number,wwol.number, true), 0, 0);
							hasModifcation = true;
							w--;
						}

					}
				}

			}

		}
		
		return hasModifcation;
		
		
	}

	private int getWinnerNumber(double i, double j, boolean ifHuman) {
		if(ifHuman){
			return (int) Math.floor((i/(i+j))*(i+j));
		} else {
			return (int) Math.floor((i/(i+j))*i);
		}
		
	}

	private boolean ifWin(float i, float j) {
		return i/(i+j) > 0.5;
	}

	private String print(ArrayList<Group> list_werewolves2) {
		String result = "";
		for(Group elem : list_werewolves2){
			result += elem.toString();
		}
		return result;
	}

	/**
	 * @return the list_werewolves
	 */
	public ArrayList<Group> getList_werewolves() {
		return list_werewolves;
	}

	/**
	 * @return the list_vampires
	 */
	public ArrayList<Group> getList_vampires() {
		return list_vampires;
	}

	/**
	 * @return the map_matrix
	 */
	public int[][][] getMap_matrix() {
		return map_matrix;
	}

	public int[] getIdleMove(int role, int x, int y) {
		int[] gravity = getEnemiesGravity(role,x,y);
		
		if(gravity == null){
			return randomMove(x, y);
		}
		
		int gx = gravity[0];
		int gy = gravity[1];
		
		Const.print("[IDLE MISSION] " + gx + " " + gy);
		
		//sur la même colonne
		if(x == gx){
			//si on est sous le centre de gravité
			if(y>gy){
				if(y!=maxY){
					if(x>maxX/2){
						return new int[]{x-1, y+1};
					} else {
						return new int[]{x+1, y+1};
					}
				}
			//au dessus du centre
			} else if(y<gy){
				if(y!=0){
					if(x>maxX/2){
						return new int[]{x-1, y-1};
					} else {
						return new int[]{x+1, y-1};
					}
				}
			}
		// sur la même ligne
		} else if(y == gy){
			//�  gauche du centre
			if(x<gx){
				if(x!=0){
					if(y>maxY/2){
						return new int[]{x-1, y-1};
					} else {
						return new int[]{x-1, y+1};
					}
				}
			//�  droite du centre
			} else if(x>gx){
				if(x!=maxX){
					if(y>maxY/2){
						return new int[]{x+1,y-1};
					} else {
						return new int[]{x+1,y+1};
					}
				}
			}
		// le centre est �  notre droite 
		} else if(x<gx){
			// le centre est �  notre droite et en haut
			if(y>gy){
				if(x!= 0 && y != maxY){
					if(x>maxY-y){
						return new int[]{x-1,y};
					} else {
						return new int[]{x,y+1};
					}
				}
			//le centre est en bas �  notre droite
			} else if(y<gy){
				if(x!= 0 && y != 0){
					if(x>y){
						return new int[]{x-1,y};
					} else {
						return new int[]{x,y-1};
					}
				}
			}
		
			
		// centre �  gauche de nous
		} else if(x>gx){
			
			// le centre est �  notre gauche en haut
			if(y>gy){
				if(x!= maxX && y != maxY){
					Const.print(x+" "+maxX+" "+y+" "+maxY);
					if(maxX-x>maxY-y){
						return new int[]{x+1,y};
					} else {
						return new int[]{x,y+1};
					}
				}
			
				//le centre est �  gauche et en bas 
			} else if(y<gy){
				if(maxX-x != 0 && y != 0){
					if(maxX-x>y){
						return new int[]{x+1,y};
					} else {
						return new int[]{x,y-1};
					}
				}
			}
		}
		
		return randomMove(x,y);
		
	}

	private int[] randomMove(int x, int y) {
		Const.print("[IDLE MISSION] random move");
		if(x<maxX){
			return new int[]{x+1,y};
		} else {
			return new int[]{x-1,y};
		}
	}

	private int[] getEnemiesGravity(int role, int x, int y) {
		int xt = 0;
		int yt = 0;
		int number = 0;
		int total = 0;
		for(Group enemy : getEnemyList(role)){
			if(Math.abs(enemy.x - x )<= 2 && Math.abs(enemy.y - y) <=2){
				xt += enemy.x * enemy.number;
				yt += enemy.y * enemy.number;
				total += enemy.number;
				number ++;
			}
		}
		if(number == 0){
			return null;
		}
		return new int[]{xt/(number*total), yt/(number*total)};
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[MAP STATE][humans=" + print(list_humans) + " werewolves="
				+ print(list_werewolves) + " vampires=" + print(list_vampires)
				+", sum="
				+ Arrays.toString(sum) + "]";
	}

}
