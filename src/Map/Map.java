package Map;

import tools.Const;

// Generic map class

public abstract class Map {

	public int role = -1;
	private int startX;
	private int startY;

	// indicates the human homes' places
	public void setHumans(int n, byte[] trame) {
		for(int i=0; i<n;i++){
			addhumans((int)trame[i*2+0], (int)trame[i*2+1]);
		}
		
	}

	//not used
	private void addhumans(int x, int y) {
		Const.print("add humans : " + x + " " + y);
	}

	//set the start case for us
	public void setStartCase(byte[] trame) {
		this.startX = (int)trame[0];
		this.startY = (int)trame[1];
		
		Const.print("get start case : " + (int)trame[0] + (int)trame[1]);
		//trame contient X Y, coordonn閑s de votre maison
		
	}

	//update the state of the indicated case in the received byte array
	public void update(int n, byte[] trame) {
		for(int i=0; i<n;i++){
			updateCase((int)trame[i*5+0],(int)trame[i*5+1],(int)trame[i*5+2],(int)trame[i*5+3],(int)trame[i*5+4]);
			Const.print(String.format("update case : (%d,%d), hum : %d, vampires : %d, werewolves : %d", (int)trame[i*5+0], (int)trame[i*5+1],(int)trame[i*5+2],(int)trame[i*5+3],(int)trame[i*5+4]));
			
			if(role == -1){
				//si les coordonn閑s sont 間ales � la case de d閜art et que le role n'est pas d閒ini : 
				if((int)trame[i*5+0] == startX && (int)trame[i*5+1] == startY){
					if((int)trame[i*5+3]>0){
						role = Const.VAMPIRE;
						Const.print("----You play vampires with " + (int)trame[i*5+3] + " vampires----");
					} else if((int)trame[i*5+4]>0){
						role = Const.WWOLF;
						Const.print("----You play werewolves with " + (int)trame[i*5+4] + "wolves----");
					}
				}
				
			}
			
		}
		
		
	}

	// X,Y,humanNB, vampires, werewolves
	abstract void updateCase(int x, int y, int hum, int vamp, int were);

}
