package Graph;

import java.util.ArrayList;

public class GraphNode {
	private ArrayList<GraphNode> sons = new ArrayList<>();
	private GraphNode parent;
	protected int floor;
	
	public GraphNode(GraphNode parent){
		this.parent = parent;
	}
	public GraphNode(){
	}
	
	public void addNeighbour(GraphNode node){
		sons.add(node);
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public GraphNode getParent() {
		return parent;
	}

	public void setParent(GraphNode parent) {
		this.parent = parent;
	}
	

}
