package Graph;

import java.util.ArrayList;

import Map.DefaultMap;
import tools.Const;
import tools.Mission;

public class MMGraphNode extends GraphNode{
	
	private ArrayList<MMGraphNode> sons = new ArrayList<MMGraphNode>();
	private float score;
	private Mission[] pMission = null;
	private MMGraphNode parent;
	private int bestSon;
	private DefaultMap map;
	public boolean toRecompute = false;
	
	
	public int getRoleFromFloor(){
		return (map.role+floor)%2;
	}
	
	public int getRoleFromFloor(int role){
		return (role+floor)%2;
	}
	
	public MMGraphNode(MMGraphNode parent,Mission[] mission) {
		this.parent = parent;
		this.floor = parent.floor + 1 ;
		this.map = new DefaultMap(parent.map);
		this.score=Const.SCORE_MAX+1;
		Const.print("[INFO]"+floor + "th floor");
		this.pMission = mission;
		
		if(map.move(mission, getRoleFromFloor())){
			toRecompute = true;
		} 
	}
	
	public MMGraphNode(DefaultMap map) {
		this.map = map;
		this.score=Const.SCORE_MAX+1;
	}
	
	public void addNeighbour(MMGraphNode node){
		sons.add(node);
	}
	
	
	public void computeRealScore(){
		int we = (int) map.getSum()[map.role];
		int they = (int) map.getSum()[(1+map.role)%2];
		if(we <= 0){
			score = Const.SCORE_MIN;
		} else if (they <=0){
			score = Const.SCORE_MAX;
		} else {
			float difference = we - they;
//			this.score = difference;
//			Const.print(String.format("difference : %d (%d - %d)", (int)difference,we, they ));
			
			float local = map.getLocalScore();
			float distance = map.getDistanceScore();
			this.score = distance + local + difference*10;
			Const.print(String.format("difference : %d (%d - %d), local : %d, distance : %d ", (int)difference,we, they, (int)local,(int)distance ));
			
		}
		
		//TODO : improve the scoring heuristic
	}

	public float getScore(int reachedFloor) {
		if(score == Const.SCORE_MAX+1){
			computeScore(reachedFloor);
		}
		return score;
	}
	
	public void computeScore(int reachedFloor){
		if(floor == reachedFloor || sons.size() == 0){
			computeRealScore();
			Const.print(floor + "th floor :Computing real score from map : " +score);
		} else if(sons.size() == 1){
			Const.print(floor + "th floor :Getting only son's score :");
			score = sons.get(0).getScore(reachedFloor);
		}
		else {
			//si c'est les �tages 0,2,4... prendre le score maximal
			if(floor%2 ==0){
				Const.print(floor + "th floor : Getting Max Score");
				score = maxScore(reachedFloor);
			} else {
				Const.print(floor + "th floor : Getting Min Score");
				score = minScore(reachedFloor);
			}
		}
		
	}

	//return the max score of the node's sons and set the bestSon to the corresponding son's number
	private float maxScore(int reachedFloor) {
		float alpha = sons.get(0).getScore(reachedFloor);
		bestSon = 0;
		for(int i = 1; i < sons.size(); i++){
			
			//alpha-beta for max score
			float beta =Const.SCORE_MAX;
			for(int j=0; j<sons.get(i).sons.size();j++)
			{
				if (sons.get(i).sons.get(j).getScore(reachedFloor)< beta)
				{
					beta=sons.get(i).sons.get(j).getScore(reachedFloor);
					if (beta<alpha)
					{
						break;
					}
				}
			}
			if(sons.get(i).sons.size()==0) 
			{
				beta=sons.get(i).getScore(reachedFloor);
			}
			if(beta > alpha){
				alpha = beta;
				bestSon = i;
				}
		}
		Const.print(floor + "th floor : max : " + alpha);
		return alpha;
		
	}

	//return the min score of the node's sons and set the bestSon to the corresponding son's number
	private float minScore(int reachedFloor) {
		float beta = sons.get(0).getScore(reachedFloor);
		bestSon = 0;
		for(int i = 1; i < sons.size(); i++){
			
			//alpha-beta for min score
			float alpha =Const.SCORE_MIN;
			for(int j=0; j<sons.get(i).sons.size();j++)
			{
				if (sons.get(i).sons.get(j).getScore(reachedFloor)>alpha)
				{
					alpha=sons.get(i).sons.get(j).getScore(reachedFloor);
					if (alpha>beta)
					{
						break;
					}
				}
			}
			if(sons.get(i).sons.size()==0) 
			{
				alpha=sons.get(i).getScore(reachedFloor);
			}
			if(alpha<beta){
				beta = alpha;
				bestSon = i;
				}
		}
		Const.print(floor + "th floor : min : " + beta);
		return beta;
	}

	public MMGraphNode getBestSon() {
		return sons.get(bestSon);
	}

	public void setBestSon(int bestSon) {
		this.bestSon = bestSon;
	}

	public Mission[] getMission() {
		return pMission;
	}

	public void setMission(Mission[] mission) {
		this.pMission = mission;
	}
	
	public Mission[] getParentMission(){
		if(parent != null ) {
			return parent.getMission();
		} 
		return null;
	}

	public DefaultMap getMap() {
		return map;
	}

	public void setMap(DefaultMap map) {
		this.map = map;
	}
	/**
	 * @return the sons
	 */
	public ArrayList<MMGraphNode> getSons() {
		return sons;
	}
	
	//get the next move of the mission of this node
	public ArrayList<int[]> getNextMove() {
		ArrayList<int[]> result = new ArrayList<int[]>();
		
		for(int i = 0; i<pMission.length; i++){
			Mission m = pMission[i];
			boolean exists = false;
			for(int j = 0; j<result.size();j++){
				if(result.get(j)[0] == m.initX && result.get(j)[1] == m.initY && result.get(j)[3] == m.x && result.get(j)[4] == m.y){
					result.get(j)[2] += m.number;
					exists = true;
				}
			}
			if(!exists && (m.initX != m.x || m.initY != m.y)){
				result.add(new int[]{m.initX, m.initY, m.number, m.x, m.y});
			}
			
		}
		
		return result;
	}

	public boolean ifNeedRecompute() {
		return parent.toRecompute;
	}

}