package Graph;

import java.util.ArrayList;

import tools.Const;
import tools.Group;
import tools.Mission;
import Map.DefaultMap;

//Decision graph 

public class MMGraph {
	
	private ArrayList<MMGraphNode> nodes = new ArrayList<MMGraphNode>(); //the array which stores all of the nodes
	private int role = -1;					// the player's identity, -1 for unknown identity

	private long startTime;
	private int reachedFloor;
	
	//create the Minimax graph
	public MMGraph(DefaultMap map){
		startTime = System.currentTimeMillis();
		
		//create the first node of the graph and atttribute the player's identity to the node
		this.nodes.add(new MMGraphNode(map));
		this.role = map.role;
		
		Const.print("");
		Const.print("/////////////////////////////////////////////////");
		Const.print("//////////// START CREATING GRAPH ///////////////");
		Const.print("/////////////////////////////////////////////////");
		Const.print("");
		
		reachedFloor = 0;
		//graph creation : parcours la liste des nodes, et pour chaque node, cr��e ses fils et les ajoutent �� la fin de la liste.
		for(int i = 0; i < nodes.size(); i++){
			
			MMGraphNode node = nodes.get(i);
			//System.out.println("TIME SPEND "+(System.currentTimeMillis()-startTime)+" "+i+ " " +node.floor);
			
			//when we change the floor and the time is over, stop the graph generation
			if((System.currentTimeMillis()-startTime)>Const.LIMIT_TIME){
				System.out.println("TIME SPEND "+(System.currentTimeMillis()-startTime)+" "+i);
				return;
			}
			
			if(reachedFloor != node.getFloor()){
				System.out.println("TIME SPEND "+(System.currentTimeMillis()-startTime) + " " +node.floor+" "+i);
			}
			
			reachedFloor = node.getFloor();
					
			//compute the identity of the node
			int r = node.getRoleFromFloor();
			
			Const.print("");
			Const.print("");
			Const.print("------------------------------------------------------------");
			Const.print("\n-----------" + i + "th NODE'S SONS, ("+node.getFloor()+"th FLOOR "+")  ---------------\n");
			Const.print("------------------------------------------------------------");
			
			Const.print("[INFO] this node's role is :" + r + " (0=vampire, 1 = wwolves)");
			Const.print("[INFO] this node's map state is :" + node.getMap().toString());
			
			//stop the creation if the max floor number is reached
			if(node.getFloor() == Const.FLOOR_MAX){
				Const.print("[INFO] !!! the max floor number is reached !");
				break;
			}
			
			Mission[] pMissions = node.getParentMission();
			ArrayList<Mission[]> missions;
			DefaultMap currentMap = node.getMap();

			//if the node's parent do not have mission, or if there is a 
			//change in population number, then compute a 
			//list of possible missions and create nodes from this list
			if(pMissions == null || node.ifNeedRecompute()){
				missions = computeMissionsList(node.getMap().getAllyList(r),node.getMap(),r);
				Const.print("[MISSION] Computing new missions : there are " + missions.size() + " missions");
			
			//if the parent have mission : 
			} else {
				boolean hasAchievedMission = false;
				for(Mission m : pMissions){
					
					//if there is at least one achieved mission, recompute a list of possible missions 
					if(m.isAchieved()){
						//TODO : optimization : mark the graph node for completed missions
						hasAchievedMission = true;
						break;
					} 
					
					//for "attack enemies" missions : check if the target population has changed : if true, recompute missions, else, refresh target coordinates
					if(m.missionCode == Const.MISSION_ENEMY){
						ArrayList<Group> enemies = currentMap.getAllyList(node.getRoleFromFloor());
						if(m.targetIndex <enemies.size() && enemies.get(m.targetIndex).number == m.population){
							Group enemy = enemies.get(m.targetIndex);
							m.x = enemy.x;
							m.y = enemy.y;
						} else {
							hasAchievedMission = true;
							break;
						}
						
						
					//for "fusion" mission : check if the target population has changed : if true, recompute missions, else, refresh target coordinates
					} else if(m.missionCode == Const.MISSION_FUSION){
						ArrayList<Group> allies = currentMap.getEnemyList(node.getRoleFromFloor());
						if(m.targetIndex <allies.size() && allies.get(m.targetIndex).number == m.population){
							Group enemy = allies.get(m.targetIndex);
							m.x = enemy.x;
							m.y = enemy.y;
						} else {
							hasAchievedMission = true;
							break;
						}
					}
					
				}
				
				if(hasAchievedMission){
					Const.print("[MISSION] Recompute all missions");
					missions = computeMissionsList(node.getMap().getAllyList(r),node.getMap(),r);
				} else {
					Const.print("[MISSION] Copying parent's mission "+pMissions.length);
					missions = new ArrayList<Mission[]>();
					Mission[] missions_temp = new Mission[pMissions.length];
					for (int j = 0; j < pMissions.length; j++) {
						missions_temp[j] = new Mission(pMissions[j]);
					}
					missions.add(missions_temp);
					
				}
				
				Const.print("[MISSION] " + missions.size() + " missions for this node");
				
			}
			
			//create the node's sons from the list of missions of the node
			for(Mission[] ms : missions){
				for(Mission m : ms){
					Const.print(m.toString());
				}
				MMGraphNode n = new MMGraphNode(node,ms);
				nodes.add(n);
				node.addNeighbour(n);
			}
			//----------------------------
			Const.print("[INFO] There is " + node.getSons().size() + " sons for this node !");
		}
		
		reachedFloor ++;
		
	}
	
	//return the next move for all groups for the current turn
	public ArrayList<int[]> getNextMove(){
		Const.print(" ");
		Const.print("*************** Start searching the best move *************");
		System.out.println("get next move start TIME SPEND : "+ (System.currentTimeMillis()-startTime));
		nodes.get(0).getScore(reachedFloor);
		MMGraphNode result = nodes.get(0).getBestSon();
		System.out.println("get next move TIME SPEND : "+ (System.currentTimeMillis()-startTime));
		return result.getNextMove();
	}

	//compute a list of missions for all ally groups
	private ArrayList<Mission[]> computeMissionsList(ArrayList<Group> allyList, DefaultMap map, int role) {
		Const.print("[MISSIONS COMPUTING] computing missions for " + allyList.size() + " movable groups of people");
		ArrayList<Mission[]> missions = new ArrayList<Mission[]>();
		for(Group ally : allyList){
			missions = pairMissionLists(missions, computeMissionsList(map, ally.x,ally.y,ally.number, role));
		}
		return missions;
	}

	//return paired and single missions for the given group of ally
	private ArrayList<Mission[]> computeMissionsList(DefaultMap map, int x, int y, int number, int role){
		ArrayList<Mission[]> missions = getPairedMissions(getSingleMissions(map, x, y, number, role),number);
		return missions;
		
		//TODO : ...implement triple missions
		
	}
	
	//compute the list of possible simple missions (only one target)
	public ArrayList<Mission[]> getSingleMissions(DefaultMap map, int x, int y, int number, int role){
		ArrayList<Group> enemies = map.getEnemyList(role);
		ArrayList<Group> humans = map.getHumanList();
		ArrayList<Group> allies = map.getAllyList(role);
		ArrayList<Mission[]> missions = new ArrayList<Mission[]>();
		
		//add simple enemy missions to the mission list
		for (int i = 0; i < enemies.size(); i++) {
			//TODO : ? verify !
			if(enemies.get(i).number*1.5 <= number){
				missions.add(new Mission[]{new Mission(x,y,number,enemies.get(i).x, enemies.get(i).y, Const.MISSION_ENEMY,  (int) Math.floor(enemies.get(i).number*1.5),i,enemies.get(i).number)});
			}
		}
		
		//add simple enemy missions to the mission list
		for (int i = 0; i < allies.size(); i++) {
			if(allies.get(i).x != x && allies.get(i).y != y){
				missions.add(new Mission[]{new Mission(x,y,number,allies.get(i).x, allies.get(i).y, Const.MISSION_FUSION, number,i,allies.get(i).number)});
			}
		}
		
		//add simple human missions to the mission list
		for(Group human : humans){
			if(human.number <= number){
				missions.add(new Mission[]{new Mission(x,y,number,human.x, human.y, Const.MISSION_HUMANS, human.number)});
			}
		}
		
		Const.print("[MISSIONS COMPUTING] There are " + missions.size() + " single missions ");
		
		if(missions.size()==0){
			int[] idleMove = map.getIdleMove(role,x,y);
			missions.add(new Mission[]{new Mission(x,y,number,idleMove[0],idleMove[1] , Const.MISSION_IDLE, number)});
		}
		return missions;

	}
	
	// take a liste of simple missions (only one target) and generate a list of pairs of missions (2 parallele targets)
	// and add the second list to the first list.
	public ArrayList<Mission[]> getPairedMissions(ArrayList<Mission[]> missions, int number){
		int simpleMissions = missions.size();
		//TODO : ?copie de l'argument ?
		
		// add pairs of missions
		for(int i = 0; i < simpleMissions-1; i++){
			for(int j = i+1; j < simpleMissions; j++){
				if(missions.get(i)[0].getMinimumNumber() + missions.get(j)[0].getMinimumNumber() <= number){
					Mission a = new Mission(missions.get(i)[0]);
					Mission b = new Mission(missions.get(j)[0]);
					a.setNumber(a.minNumber);
					b.setNumber(number - a.minNumber);
					missions.add(new Mission[]{a,b});
				}
			}
		}
		Const.print("[MISSIONS COMPUTING] There are " + missions.size() + " pairs of missions ");
		
		return missions;
	}
	
	
	//pair missions list, if the first list contains only one missions list 
	// (usually the list of missions not achieved yet), then adds simply this list to all elements of the second list
	public ArrayList<Mission[]> pairMissionLists(ArrayList<Mission[]> missions1,ArrayList<Mission[]> missions2){
		// TODO : ?copie de l'argument ?
		// add pairs of missions
		if(missions2.size() == 0){
			return missions1;
		} else if(missions1.size() == 0){
			return missions2;
		}
		
		//si les listes ne sont pas vides :
		ArrayList<Mission[]> missions = new ArrayList<Mission[]>();
		for(int i = 0; i < missions1.size(); i++){
			for(int j = 0; j < missions2.size(); j++){
				if(areDifferent(missions1.get(i),missions2.get(j))){
					missions.add(concat(missions1.get(i), missions2.get(j)));
				}
			}
		}
		return missions;
	}

	private boolean areDifferent(Mission[] missions, Mission[] missions2) {
		for(Mission m : missions){
			for(Mission m2 : missions2){
				if(m.toX == m2.toX && m.toY == m2.toY){
					return false;
				}
			}
		}
		return true;
	}

	//ajout d'une liste �� une autre
	private static Mission[] concat(Mission[] first, Mission[] second) {
		
		 int l1 = first.length;
		 int l2 = second.length;
		 
		  Mission[] result = new Mission[l1 + l2];
		  for(int i = 0; i<l1; i++){
			  result[i] = new Mission(first[i]);
		  }
		  
		  for(int j = l1;j<l1+l2; j++){
			  result[j] = new Mission(second[j-l1]);
		  }
		  
//		  Arrays.copyOf(first, l1 + l2);
//		  System.arraycopy(second, 0, result, l1, l2);
		  return result;
	}
	
	
}
